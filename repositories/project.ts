import dbExec from "./index";
import {IProject} from "./schemas";
import {v1 as uuidv1} from "uuid"

export default class ProjectRepository {
  private readonly fields: string

  constructor() {
    this.fields = `id
    title
    slug
    description
    redirectUrlSuccess
    redirectUrlFailed
    createdAt
    updatedAt
    deletedAt`
  }

  public async findById(id: string) {
    const query = `query projectFindById($id: String) {
      project(where: {id: {_eq: $id}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {id})
  }

  public async create(project: IProject) {
    project.id = uuidv1()
    project.updatedAt = new Date()
    const query = `mutation projectCreate(
      $id: String,
      $title: String,
      $slug: String,
      $description: String,
      $redirectUrlSuccess: String,
      $redirectUrlFailed: String,
      $updatedAt: timestamp = ""
    ) {
      insert_project_one(object: {
        id: $id,
        title: $title,
        slug: $slug,
        description: $description,
        redirectUrlSuccess: $redirectUrlSuccess,
        redirectUrlFailed: $redirectUrlFailed,
        updatedAt: $updatedAt
      }) {
        ${this.fields}
      }
    }`

    return await dbExec(query, project)
  }

  public async update(keys: string, set: string, values: any) {
    const query = `mutation pageCreate(
      $id: String,
      $accountId: String,
      $updatedAt: timestamp = "",
      ${keys}
    ) {
      update_page(
        where: {
          id: {_eq: $id},
          accountId: {_eq: $accountId}
        },
        _set: {
          updatedAt: $updatedAt,
          ${set}
        }
      ) {
        returning {
          ${this.fields}
        }
      }
    }`

    let opts: any = {
      ...values,
      updatedAt: new Date()
    }
    return await dbExec(query, opts)
  }
}