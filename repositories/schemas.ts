interface IDefaultSchema {
  id?: string
  createdAt?: any
  updatedAt?: any
  deletedAt?: any
}

export interface IProject extends IDefaultSchema {
  title?: string
  slug?: string
  description?: string
  redirectUrlSuccess?: string
  redirectUrlFailed?: string
  metadata?: any
}

export interface ITransaction extends IDefaultSchema {
  projectId?: string
  paymentStatus?: TRANSACTION_PAYMENT_STATUS
  token?: string
  redirectUrl?: string
  grossAmount?: number
  metadata?: any
}

export enum TRANSACTION_PAYMENT_STATUS {
  PENDING = 'PENDING',
  PAID = 'PAID',
  CANCELLED = 'CANCELLED',
  ERROR = 'ERROR',
}

export interface ITransactionHistory extends IDefaultSchema {
  transactionId?: string
  paymentGatewayTransactionId?: string
  paymentGatewayTransactionStatus?: string
  paymentGatewayTransactionTime?: any
  grossAmount?: number
  status?: string
  metadata?: any
}