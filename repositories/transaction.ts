import dbExec from "./index";
import {ITransaction, ITransactionHistory} from "./schemas";
import {v1 as uuidv1} from "uuid"

export default class TransactionRepository {
  private readonly fields: string

  constructor() {
    this.fields = `id
    projectId
    token
    redirectUrl
    paymentStatus
    grossAmount
    metadata
    createdAt
    updatedAt
    deletedAt
    
    project {
      id
      title
      slug
      description
      redirectUrlSuccess
      redirectUrlFailed
      createdAt
      updatedAt
      deletedAt
    }
    
    transaction_histories{
      id
      transactionId
      paymentGatewayTransactionId
      paymentGatewayTransactionStatus
      paymentGatewayTransactionTime
      grossAmount
      status
      metadata
      createdAt
      updatedAt
      deletedAt
    }`
  }

  public async findByProjectId(projectId: string) {
    const query = `query transactionFindByProjectId($projectId: String) {
      transaction(where: {projectId: {_eq: $projectId}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {projectId})
  }

  public async findById(id: string) {
    const query = `query transactionFindById($id: String) {
      transaction(where: {id: {_eq: $id}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {id})
  }

  public async findByIdAndProjectId(id: string, projectId: string) {
    const query = `query transactionFindById($id: String, $projectId: String) {
      transaction(where: {
        _and: {
          id: {_eq: $id},
          projectId: {_eq: $projectId}
        } 
      }) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {id, projectId})
  }

  public async create(transaction: ITransaction) {
    transaction.updatedAt = new Date()
    const query = `mutation transactionCreate(
      $id: String,
      $projectId: String,
      $paymentStatus: String,
      $token: String,
      $redirectUrl: String,
      $grossAmount: bigint,
      $metadata: jsonb = {},
      $updatedAt: timestamp = ""
    ) {
      insert_transaction_one(object: {
        id: $id,
        projectId: $projectId,
        paymentStatus: $paymentStatus,
        token: $token,
        redirectUrl: $redirectUrl,
        grossAmount: $grossAmount,
        metadata: $metadata,
        updatedAt: $updatedAt
      }) {
        ${this.fields}
      }
    }`

    return await dbExec(query, transaction)
  }

  public async createHistory(transactionHistory: ITransactionHistory) {
    transactionHistory.id = uuidv1()
    transactionHistory.updatedAt = new Date()
    const query = `mutation transactionHistoryCreate(
      $id: String,
      $transactionId: String,
      $paymentGatewayTransactionId: String,
      $paymentGatewayTransactionStatus: String,
      $paymentGatewayTransactionTime: timestamp,
      $grossAmount: bigint,
      $status: String,
      $metadata: jsonb = {},
      $updatedAt: timestamp = ""
    ) {
      insert_transaction_history_one(object: {
        id: $id,
        transactionId: $transactionId,
        paymentGatewayTransactionId: $paymentGatewayTransactionId,
        paymentGatewayTransactionStatus: $paymentGatewayTransactionStatus,
        paymentGatewayTransactionTime: $paymentGatewayTransactionTime,
        grossAmount: $grossAmount,
        status: $status,
        metadata: $metadata,
        updatedAt: $updatedAt,
      }) {
        id
        transactionId
        paymentGatewayTransactionId
        paymentGatewayTransactionStatus
        paymentGatewayTransactionTime
        grossAmount
        status
        metadata
        createdAt
        updatedAt
      }
    }`

    return await dbExec(query, transactionHistory)
  }

  public async update(keys: string, set: string, values: any) {
    const query = `mutation transactionUpdate(
      $id: String,
      $updatedAt: timestamp = "",
      ${keys}
    ) {
      update_transaction(
        where: {
          id: {_eq: $id}
        },
        _set: {
          updatedAt: $updatedAt,
          ${set}
        }
      ) {
        returning {
          ${this.fields}
        }
      }
    }`

    let opts: any = {
      ...values,
      updatedAt: new Date()
    }
    return await dbExec(query, opts)
  }
}