import ProjectRepository from "repositories/project";

export default class ProjectService {
  private readonly projectRepository: ProjectRepository

  constructor() {
    this.projectRepository = new ProjectRepository()
  }

  public async verify(projectId: string) {
    const result: any = await this.projectRepository.findById(projectId)
    if (result.errors) {
      console.dir({ method: 'ProjectService.verify', when: 'this.projectRepository.findById', error: {...result.errors, projectId} })
      result.errors = [{message: 'Something went wrong.'}]
      return result
    }

    if (!result.data.project.length) {
      result.errors = [{message: 'Unauthorized.'}]
      return result
    }

    result.data = result.data.project[0]
    return result
  }
}