import {cookieKey} from "common/constant";
import jwt from "jsonwebtoken"
import moment from "moment";
import exp from "constants";

export default class AuthService {
  private readonly cookies: any
  private readonly cookie: string
  private readonly token: any

  constructor(cookies: any) {
    this.cookies = cookies
    this.cookie = this.cookies[cookieKey]
    this.token = jwt.verify(this.cookie, process.env.SECRET_HASH!)
  }

  public getToken() {
    return this.token.id && this.token.fullName && this.token.subscriptionType ? this.token : undefined
  }

  public getUserId() {
    return this.token.id || undefined
  }

  public getFullName() {
    return this.token.fullName || undefined
  }

  public getSubscriptionType() {
    return this.token.subscriptionType || undefined
  }

  public getSubscriptionExpiredDate() {
    return this.token.expiredAt ? moment(this.token.expiredAt).format('DD-MM-YYYY') : ''
  }

  public isSubscriptionExpired() {
    if (!this.token.expiredAt) return true

    const today: any = moment()
    const expiredAt: any = moment(this.token.expiredAt)
    return expiredAt.diff(today, 'days') < 0 ? true : false
  }
}