// @ts-ignore
import Midtrans from "midtrans-client"
import {IProject, ITransaction, ITransactionHistory, TRANSACTION_PAYMENT_STATUS} from "repositories/schemas";
import TransactionRepository from "repositories/transaction";

export default class TransactionService {
  private readonly transactionRepository: TransactionRepository
  private readonly midtrans: any

  constructor() {
    this.transactionRepository = new TransactionRepository()
    this.midtrans = new Midtrans.Snap({
      isProduction: process.env.MIDTRANS_ENV == 'production' ? true : false,
      clientKey: process.env.MIDTRANS_CLIENT_KEY,
      serverKey: process.env.MIDTRANS_SERVER_KEY
    })
  }

  public getPaymentStatus(transactionStatus: string) {
    let status: TRANSACTION_PAYMENT_STATUS

    switch (transactionStatus.toLowerCase()) {
      case 'capture':
        status = TRANSACTION_PAYMENT_STATUS.PAID
        break
      case 'settlement':
        status = TRANSACTION_PAYMENT_STATUS.PAID
        break
      case 'pending':
        status = TRANSACTION_PAYMENT_STATUS.PENDING
        break
      case 'deny':
        status = TRANSACTION_PAYMENT_STATUS.CANCELLED
        break
      case 'cancel':
        status = TRANSACTION_PAYMENT_STATUS.CANCELLED
        break
      case 'expire':
        status = TRANSACTION_PAYMENT_STATUS.CANCELLED
        break
      default:
        status = TRANSACTION_PAYMENT_STATUS.PENDING
    }

    return status
  }

  public async get(id: string, projectId: string) {
    const result: any = await this.transactionRepository.findByIdAndProjectId(id, projectId)
    if (result.errors) {
      console.dir({ method: 'TransactionService.get', when: 'this.transactionRepository.findByIdAndProjectId', error: 'Failed to retrieve transaction', detail: {...result.errors, id, projectId} })
      return result
    }

    result.data = result.data.transaction.length ? result.data.transaction[0] : {}
    return result
  }

  public create(project: IProject, transaction: ITransaction, cb: Function) {
    let _this = this
    const transactionDetail: any = {
      transaction_details: {
        order_id: transaction.id,
        gross_amount: transaction.grossAmount
      },
      customer_details: {
        first_name: transaction.metadata.customer.firstName,
        last_name: transaction.metadata.customer.lastName,
        email: transaction.metadata.customer.email,
        phone: transaction.metadata.customer.phone
      },
      custom_field1: project.id,
      custom_field2: transaction.id,
      metadata: {
        project_id: project.id,
        client_order_id: transaction.id
      }
    }

    this.midtrans.createTransaction(transactionDetail)
      .then(async (result: any) => {
        let error: string
        const {token, redirect_url: redirectUrl} = result
        if (token.length && redirectUrl.length) {
          transaction.paymentStatus = TRANSACTION_PAYMENT_STATUS.PENDING
          transaction.projectId = project.id
          transaction.token = token
          transaction.redirectUrl = redirectUrl
          transaction.metadata = transactionDetail

          let trxResult: any = await _this.transactionRepository.create(transaction)
          if (trxResult.errors) {
            error = trxResult.errors[0].message
            console.dir({ method: 'TransactionService.create', when: 'this.transactionRepository.create', error, detail: trxResult.errors })
            return cb(error)
          }
          cb(undefined, result)
        } else {
          console.dir({ method: 'TransactionService.create', when: 'token.and.redirect_url', error: 'Not receiving response token and redirect_url from Midtrans when creating a snap transaction.' })
          return cb('Something went wrong.')
        }
      })
      .catch((err: any) => {
        console.dir({ method: 'TransactionService.create', when: 'callback.catch', error: 'Error when creating a snap transaction to Midtrans.', detail: err })
        const error: string = err.ApiResponse?.error_messages.length ? err.ApiResponse?.error_messages[0] : 'Something went wrong.'
        cb(err)
      })
  }

  public async updateStatus(data: any) {
    let transactionHistory: ITransactionHistory = {
      transactionId: data.order_id,
      paymentGatewayTransactionId: data.transaction_id,
      paymentGatewayTransactionStatus: data.transaction_status,
      paymentGatewayTransactionTime: data.transaction_time,
      status: ['200', '201'].indexOf(data.status_code.toString()) > -1 ? 'SUCCESS' : 'FAILED',
      grossAmount: parseInt(data.gross_amount),
      metadata: data
    }

    let result: any = await this.transactionRepository.createHistory(transactionHistory)
    if (result.errors) {
      console.dir({ method: 'TransactionService.updateStatus', when: 'this.transactionRepository.createHistory', error: 'Failed to create transaction history', detail: {...result.errors, transactionHistory} })
      return result
    }

    if (this.getPaymentStatus(data.transaction_status) != TRANSACTION_PAYMENT_STATUS.PENDING) {
      const status: TRANSACTION_PAYMENT_STATUS = this.getPaymentStatus(data.transaction_status)
      const keys: string = `$paymentStatus: String`
      const set: string = `paymentStatus: $paymentStatus`
      const transaction: ITransaction = {
        id: transactionHistory.transactionId,
        paymentStatus: status
      }

      result = await this.transactionRepository.update(keys, set, transaction)
      if (result.errors) {
        console.dir({ method: 'TransactionService.updateStatus', when: 'this.transactionRepository.update', error: `Failed to update transaction`, detail: {...result.errors, transaction} })
        return result
      }
    }

    result.data = transactionHistory
    return result
  }
}