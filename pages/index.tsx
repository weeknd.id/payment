import PageHead from "components/head";
import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import {isUnauthorized} from "util/page-server-side";
import FooterIcon from "components/icon/footer";

export default function Home({ baseUrl }: any) {
  return (
    <div>
      <PageHead title={`Prototype`} />
      <main>
        <div className={`container mx-auto`}>
          <FooterIcon />
        </div>
      </main>
    </div>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies: any = parseCookies(ctx)

  if (!isUnauthorized(cookies)) {
    return {
      redirect: {
        destination: '/dashboard',
        permanent: false
      }
    }
  }

  return {
    props: {
      baseUrl: process.env.BASE_URL
    }
  }
}