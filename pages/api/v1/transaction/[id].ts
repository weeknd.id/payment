import {NextApiRequest, NextApiResponse} from "next";
import TransactionService from "services/transaction";
import {extractProjectIdFromReqHeaders} from "util/extract";
import ProjectService from "services/project";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const projectId: string = extractProjectIdFromReqHeaders(req.headers)

  const projectService: ProjectService = new ProjectService()
  let result: any = await projectService.verify(projectId)
  if (result.errors) return res.status(401).json({ success: false, error: result.errors[0].message })

  const {id}: any = req.query
  const transactionService: TransactionService = new TransactionService()
  const {errors, data}: any = await transactionService.get(id, projectId)
  if (errors && errors.length) {
    return res.status(500).json({ success: false, error: errors[0].message })
  }

  res.status(200).json({ success: true, data })
}