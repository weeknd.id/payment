import {NextApiRequest, NextApiResponse} from "next";
import TransactionService from "services/transaction";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  if (!req.headers['user-agent']) return res.status(401).json({ success: false, error: 'Unauthorized.' })
  if (req.headers['user-agent'] && req.headers['user-agent']?.toLowerCase() != 'veritrans') return res.status(401).json({ success: false, error: 'Unauthorized.' })

  const transactionService: TransactionService = new TransactionService()
  const {errors, data}: any = await transactionService.updateStatus(req.body)
  if (errors && errors.length) {
    return res.status(500).json({ success: false, error: errors[0].message })
  }

  res.status(200).json({ success: true, data})
}