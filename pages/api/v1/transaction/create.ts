import {NextApiRequest, NextApiResponse} from "next";
import TransactionService from "services/transaction";
import ProjectService from "services/project";
import {extractProjectIdFromReqHeaders} from "util/extract";
import {IProject, ITransaction} from "repositories/schemas";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const projectId: string = extractProjectIdFromReqHeaders(req.headers)
  if (projectId == '') return res.status(500).json({ success: false, error: 'Unauthorized' })

  const projectService: ProjectService = new ProjectService()
  const result: any = await projectService.verify(projectId)
  if (result.errors) return res.status(401).json({ success: false, error: result.errors[0].message })

  const project: IProject = result.data
  const transaction: ITransaction = req.body

  await new Promise((resolve) => {
    const transactionService: TransactionService = new TransactionService()
    transactionService.create(project, transaction, (err: any, result: any) => {
      if (err) res.status(500).json({ success: false, error: err })
      else res.status(200).json({ success: true, data: { token: result.token, redirectUrl: result.redirect_url } })
      resolve(() => {})
    })
  })
}