import {NextPageContext} from "next";
import TransactionRepository from "repositories/transaction";
import {useEffect} from "react";

export default function ErrorPage ({ errors, transaction, orderId, status }: any) {
  useEffect(() => {
    if (!errors && transaction && transaction.project && transaction.project.redirectUrlSuccess) {
      setTimeout(() => {
        window.location.href = transaction.project.redirectUrlFailed
      }, 3000)
    }
  }, [])

  return (
    <div className={`bg-repeat w-full h-screen flex justify-center items-center`} style={{backgroundImage: `url('//d2f3dnusg0rbp7.cloudfront.net/snap/assets/bg-5325379f79c74e499f0f8cba89e2a269ce58cb7fd0d175645e2fe3f731b46f64.jpg')`}}>
      <div className={`bg-white w-3/12 p-5 border rounded-lg`}>
        <div className={`text-center`}>
          <div className={`uppercase`}>Order ID</div>
          <div className={`font-semibold text-xl mb-5`}>{orderId}</div>
          <div className={`uppercase`}>Status</div>
          <div className={`font-semibold text-xl uppercase mb-7`}>{transaction ? transaction.paymentStatus : status}</div>
          <div>Terjadi kesalahan pada sistem.<br/>Mohon menghubungi customer service dan screenshot halaman ini.</div>
        </div>
      </div>
    </div>
  )
}

export async function getServerSideProps (ctx: NextPageContext) {
  const {query}: any = ctx
  const {order_id: orderId, transaction_status: status} = query

  const transactionRepository: TransactionRepository = new TransactionRepository()
  const {errors, data}: any = await transactionRepository.findById(orderId)

  return {
    props: {
      errors: errors ? errors : null,
      transaction: data.transaction && data.transaction.length ? data.transaction[0] : null,
      orderId,
      status
    }
  }
}