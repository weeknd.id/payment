const Logo = () => (
  <div className={`text-indigo-600 text-2xl uppercase`}>Marketify</div>
)

export default Logo