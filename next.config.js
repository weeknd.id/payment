module.exports = {
  reactStrictMode: true,

  webpack: function (config) {
    config.module.rules.push({
      test: /\.ya?ml$/,
      use: 'js-yaml-loader'
    })

    return config
  },

  images: {
    domains: ['placeimg.com']
  },

  env: {}
}
