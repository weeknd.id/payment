export const extractProjectIdFromReqHeaders = (headers: any) => {
  return headers['authorization'] ? headers['authorization'].toString().replace('Bearer ', '') : ''
}