import Fetch from "./fetch";

export default class Graphql {
  private readonly env: any
  private readonly query: string
  private readonly variables: any

  constructor(env: any, query: string, variables: any) {
    this.env = env
    this.query = query
    this.variables = variables
  }

  public async execute() {
    const headers: any = {
      'content-type': 'application/json',
      'x-hasura-admin-secret': this.env.GRAPHQL_SECRET
    }

    const data: any = {
      query: this.query,
      variables: this.variables
    }

    return await Fetch('POST', this.env.GRAPHQL_URL, data, headers)
  }
}